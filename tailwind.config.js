const colors = require('tailwindcss/colors')
/** @type {import('tailwindcss').Config} */
export default {
  content: ["./**/*.{html,js}"],
  theme: {
    extend: {},
    colors: {
      ...colors,
      'brand-color-100': '#B9E855',
      'brand-color-900': '#003600',
    },
    screens: {
      'xs': {'min': '320px', 'max': '576px'},
      'sm': {'min': '576px', 'max': '767px'},
      'md': {'min': '768px', 'max': '991px'},
      'lg': {'min': '992px', 'max': '1199px'},
      'xl': {'min': '1200px'},
  },
  },
  plugins: [],
}

